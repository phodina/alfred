# Alfred - The humble butler

This project is about replacing proprietary main board in a vaccum cleaner - Mi Robot Vacuum Mop 1C.

The idea is to design a board with connector for Raspberry Pi 4/5 Compute module
or Rockchip SOQuartz that's also pin to pin compatible.

The low level stuff like driving the motors and reading the sensors will be
task for an MCU such as RP2040 or STM32F4.

As this is replacement additional sensors will be attached such as ToF or UWB.

![Xiaomi Mi Robot Vaccum Mop 1C](images/xiaomi_mainboard.jpg)

# TODO
- [Add Raspberry Pi 4 CM into Kicad](https://gitlab.com/kicad/libraries/kicad-symbols/-/merge_requests/4159)
- Redraw the board outline in Kicad
